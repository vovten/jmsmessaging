﻿Программа предназначения для работы со встроенным JMS сервером WebSphere Liberty

1.Конфигурирование
Для начала работы открываем файл конфигурации '.\conf\application.properties' и задаем значения provider.endpoint, queue.name, topic.name.
Для отправки текстового сообщения, открываем файл '.\conf\message.xml' и помещаем туда текст сообщения. 
Для отправки объекта, помещаем в каталог './conf/ObjectMessages' JAR-ник содержащий классы отправляемых объектов, а в настройках
object.message.jar.name и object.message.class.name указываем имя JAR-ника и имя класса отправляемого объекта соответственно.

2. Отправка сообщений
Для очереди: команда для отправки текстового сообщения         - 'java -jar jmsMessaging.jar -qst'
Для очереди: команда для отправки сообщения содержащего объект - 'java -jar jmsMessaging.jar -qso'
Для топика: команда для отправки текстового сообщения          - 'java -jar jmsMessaging.jar -tst'
Для топика: команда для отправки сообщения содержащего объект  - 'java -jar jmsMessaging.jar -tso'

3. Получение сообщений   
Для очереди: команда для получения сообщения                   - 'java -jar jmsMessaging.jar -qr'
Для топика: команда для получения сообщения                    - 'java -jar jmsMessaging.jar -tr'

4. Ссылка на репозиторий
https://bitbucket.org/vovten/jmsmessaging