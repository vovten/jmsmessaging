package ru.vovten.jms;

import com.ibm.websphere.sib.api.jms.JmsFactoryFactory;
import ru.vovten.AppProperties;

import javax.jms.Destination;
import javax.jms.JMSException;

/**
 * The class for interaction with Liberty JMS server through a topic
 *
 * @author sbt-aleshkov-vyu
 * @created 11.08.2017.
 */
public class TopicJmsMessaging extends JmsMessaging {
    public TopicJmsMessaging(String destinationName) {
        super(destinationName);
    }

    @Override
    protected Destination createDestination(JmsFactoryFactory factory) throws JMSException {
        return factory.createTopic(PROPERTIES.getProperty(AppProperties.TOPIC_NAME));
    }
}
