package ru.vovten.jms;

/**
 * Destination type of JMS
 *
 * @author sbt-aleshkov-vyu
 * @created 11.08.2017.
 */
public enum DestinationType {
    QUEUE("queue"),
    TOPIC("topic");

    private String description;

    DestinationType(String description) {
        this.description = description;
    }

    public String description() {
        return description;
    }
}
