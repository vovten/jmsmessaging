package ru.vovten.jms;

import com.ibm.websphere.sib.api.jms.JmsFactoryFactory;
import ru.vovten.AppProperties;

import javax.jms.Destination;
import javax.jms.JMSException;

/**
 * The class for interaction with Liberty JMS server through a queue
 *
 * @author sbt-aleshkov-vyu
 * @created 11.08.2017.
 */
public class QueueJmsMessaging extends JmsMessaging {

    public QueueJmsMessaging(String destinationName) {
        super(destinationName);
    }

    @Override
    protected Destination createDestination(JmsFactoryFactory factory) throws JMSException {
        return factory.createQueue(PROPERTIES.getProperty(AppProperties.QUEUE_NAME));
    }
}
