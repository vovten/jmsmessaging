package ru.vovten.jms;

import com.ibm.websphere.sib.api.jms.JmsConnectionFactory;
import com.ibm.websphere.sib.api.jms.JmsFactoryFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.vovten.AppProperties;
import ru.vovten.jms.message.JmsMessage;

import javax.jms.*;
import java.util.Properties;

/**
 * Base class for interaction with WebSphere Liberty JMS server
 *
 * Created by Vladimir Aleshkov on 06.05.2017.
 */
public abstract class JmsMessaging {
    private static final Logger logger = LoggerFactory.getLogger(JmsMessaging.class);
    static final Properties PROPERTIES = AppProperties.getInstance().getProperties();
    private String destinationName;

    public JmsMessaging(String destinationName) {
        this.destinationName = destinationName;
    }

    /**
     * Creates an instance of JmsMessaging service depending on the type of destination
     *
     * @param destType destination type
     * @return an instance of JmsMessaging
     */
    public static JmsMessaging create(DestinationType destType) {
        switch (destType) {
            case QUEUE:
                return new QueueJmsMessaging(PROPERTIES.getProperty(AppProperties.QUEUE_NAME));

            case TOPIC:
                return new TopicJmsMessaging(PROPERTIES.getProperty(AppProperties.TOPIC_NAME));

            default:
                throw new IllegalArgumentException("Unsupported destination type");
        }
    }

    /**
     * Sends a message to a JMS destination
     *
     * @param jmsMessage jms message
     */
    public void send(JmsMessage jmsMessage) {
        Connection connection = null;
        try {
            JmsFactoryFactory jmsFactory = JmsFactoryFactory.getInstance();
            connection = createConnectionFactory(jmsFactory).createConnection();
            connection.start();

            Session session = connection.createSession(false, 1);
            Message message = jmsMessage.create(session);
            logger.info("Sending message...." + message.toString());
            session.createProducer(createDestination(jmsFactory)).send(message);
            logger.info("The message has been sent");
        } catch (Exception e) {
            logger.error("Failed to send message", e);
        } finally {
            if (connection != null) try {
                connection.close();
            } catch (JMSException ignored) {}
        }
    }

    /**
     * Receives a message from a JMS destination
     *
     * @return received message
     */
    public Message receive() {
        Connection connection = null;
        try {
            JmsFactoryFactory jmsFactory = JmsFactoryFactory.getInstance();
            connection = createConnectionFactory(jmsFactory).createConnection();
            connection.start();

            return connection.createSession(false, 1).createConsumer(createDestination(jmsFactory)).receive();
        } catch (JMSException e) {
            logger.error("Failed to receive message", e);
        } finally {
            if (connection != null) try {
                connection.close();
            } catch (JMSException ignored) {}
        }
        return null;
    }

    /**
     * JMS destination name
     *
     * @return destination name
     */
    public String getDestinationName() {
        return destinationName;
    }

    private JmsConnectionFactory createConnectionFactory(JmsFactoryFactory factory) throws JMSException {
        JmsConnectionFactory connectionFactory = factory.createConnectionFactory();
        connectionFactory.setBusName("myBus");
        connectionFactory.setProviderEndpoints(PROPERTIES.getProperty(AppProperties.ENDPOINT));
        return connectionFactory;
    }

    /**
     * Creates a JMS destination
     *
     * @param factory jms factory
     * @return destination
     * @throws JMSException
     */
    protected abstract Destination createDestination(JmsFactoryFactory factory) throws JMSException;
}
