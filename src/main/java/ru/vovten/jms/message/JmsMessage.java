package ru.vovten.jms.message;

import ru.vovten.AppProperties;

import javax.jms.IllegalStateException;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.io.IOException;
import java.util.Properties;

import static ru.vovten.AppProperties.JMS_CORRELATION_ID;
import static ru.vovten.AppProperties.JMS_TYPE;

/**
 * Base class for a JMS message
 *
 * Created by Vladimir Aleshkov on 09.05.2017.
 */
public abstract class JmsMessage {
    private static final Properties PROPERTIES = AppProperties.getInstance().getProperties();

    public static JmsMessage createJmsMessage(MessageType type) throws IOException {
        switch (type) {
            case TEXT:
                return new TextJmsMessage();

            case OBJECT:
                return new ObjectJmsMessage();

            default:
                throw new IllegalArgumentException("Unsupported message type");
        }
    }
    /**
     * Creates a JMS message
     * @param session jms connection session
     * @return JMS message
     * @throws JMSException
     * @throws IOException
     */
    public abstract Message create(Session session) throws Exception;

    /**
     * Method sets the custom properties
     * @param message a jms message
     * @return a jms message with custom properties
     * @throws JMSException
     */
    protected Message setMsgProperties(Message message) throws JMSException {
        //Standard message properties
        message.setJMSCorrelationID(PROPERTIES.getProperty(JMS_CORRELATION_ID));
        message.setJMSType(PROPERTIES.getProperty(JMS_TYPE));

        //Custom message properties
        for (int i = 0; i < PROPERTIES.size(); i++) {
            String propKey = AppProperties.MESSAGE_PROPERTY + i;

            if (PROPERTIES.containsKey(propKey)) {
                String[] keyValueType = PROPERTIES.getProperty(propKey).split("\\|");
                String key = keyValueType[0].trim();

                if (keyValueType.length < 3) {
                    throw new IllegalStateException(String.format("Value or type of property '%s' doesn't set", key));
                }

                String value = keyValueType[1].trim();
                String type = keyValueType[2].trim().toLowerCase();

                switch (type) {
                    case "string":
                        message.setStringProperty(key, value);
                        break;

                    case "int":
                        message.setIntProperty(key, Integer.parseInt(value));
                        break;

                    case "boolean":
                        message.setBooleanProperty(key, Boolean.parseBoolean(value));
                        break;

                    case "byte":
                        message.setByteProperty(key, Byte.parseByte(value));
                        break;

                    case "double":
                        message.setDoubleProperty(key, Double.parseDouble(value));
                        break;

                    case "float":
                        message.setFloatProperty(key, Float.parseFloat(value));
                        break;
                }
            }
        }
        return message;
    }
}