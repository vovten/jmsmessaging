package ru.vovten.jms.message;

import ru.vovten.AppProperties;
import ru.vovten.util.JarClassLoader;

import javax.jms.Message;
import javax.jms.Session;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import static java.lang.System.getProperty;
import static ru.vovten.AppProperties.*;

/**
 * Object message JMS
 *
 * @author sbt-aleshkov-vyu
 * @created 21.08.2017.
 */
public class ObjectJmsMessage extends JmsMessage {
    private static final Properties PROPERTIES = AppProperties.getInstance().getProperties();

    @Override
    public Message create(Session session) throws Exception {
        return setMsgProperties(session.createObjectMessage(createObject()));
    }

    private Serializable createObject() throws ClassNotFoundException, IllegalAccessException, InstantiationException, MalformedURLException {
        JarClassLoader classLoader = new JarClassLoader(new URL[]{});
        classLoader.addJar(getProperty("user.dir") + JAR_PATH + PROPERTIES.getProperty(JAR_NAME));
        return (Serializable) classLoader.loadClass(PROPERTIES.getProperty(OBJECT_MSG_CLASS_NAME)).newInstance();
    }
}
