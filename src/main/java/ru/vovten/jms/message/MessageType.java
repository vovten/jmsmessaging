package ru.vovten.jms.message;

/**
 * Message type of JMS
 *
 * @author sbt-aleshkov-vyu
 * @created 21.08.2017.
 */
public enum MessageType {
    OBJECT,
    TEXT
}
