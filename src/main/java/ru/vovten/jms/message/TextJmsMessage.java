package ru.vovten.jms.message;

import javax.jms.Message;
import javax.jms.Session;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import static ru.vovten.AppProperties.TEXT_MSG_XML_PATH;

/**
 * Text message JMS
 *
 * @author sbt-aleshkov-vyu
 * @created 21.08.2017.
 */
public class TextJmsMessage extends JmsMessage {
    private String text;

    /**
     * Message
     * @throws IOException
     */
    public TextJmsMessage() throws IOException {
        this.text = textFromFile(TEXT_MSG_XML_PATH, Charset.defaultCharset());
    }

    @Override
    public Message create(Session session) throws Exception {
        return setMsgProperties(session.createTextMessage(text));
    }

    @Override
    public String toString() {
        return text;

    }

    private String textFromFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}
