package ru.vovten;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.JMSException;
import java.io.IOException;

import static ru.vovten.CommandExecutor.INFO_MSG;

/**
 * Entry point of WebSphere Liberty JMS messaging
 *
 * Created by Vladimir Aleshkov on 07.05.2017.
 */
public class Main {
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException, JMSException {
        if (args.length == 0) {
            log.info(INFO_MSG);
            return;
        }
        CommandExecutor.execute(args[0]);
    }
}
