package ru.vovten.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * The class loader from Jar
 * @author sbt-aleshkov-vyu
 * @created 22.08.2017.
 */
public class JarClassLoader extends URLClassLoader {
    public JarClassLoader(URL[] urls) {
        super(urls);
    }

    public void addJar(String path) throws MalformedURLException {
        String urlPath = "jar:file:" + path + "!/";
        addURL(new URL(urlPath));
    }
}
