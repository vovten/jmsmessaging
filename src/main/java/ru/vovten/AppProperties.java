package ru.vovten;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Vladimir Aleshkov on 07.05.2017.
 */
public class AppProperties {
    public static final String PATH_TO_PROPERTIES = "conf/application.properties";
    public static final String JAR_PATH = "/conf/ObjectMessages/";
    public static final String TEXT_MSG_XML_PATH = "conf/message.xml";
    public static final String ENDPOINT = "provider.endpoint";
    public static final String QUEUE_NAME = "queue.name";
    public static final String TOPIC_NAME = "topic.name";
    public static final String MESSAGE_PROPERTY = "message.property.";
    public static final String JAR_NAME = "object.message.jar.name";
    public static final String OBJECT_MSG_CLASS_NAME = "object.message.class.name";
    public static final String JMS_CORRELATION_ID = "JMSCorrelationID";
    public static final String JMS_TYPE = "JMSType";

    private static final Logger logger = LoggerFactory.getLogger(AppProperties.class);
    private static final AppProperties INSTANCE = new AppProperties();

    private Properties properties;

    private AppProperties() {
        init();
    }

    public static AppProperties getInstance() {
        return INSTANCE;
    }

    public Properties getProperties() {
        return properties;
    }

    private void init() {
        try (FileInputStream fis = new FileInputStream(PATH_TO_PROPERTIES)) {
            properties = new Properties();
            properties.load(fis);
        } catch (IOException e) {
            logger.error("Ошибка во время чтения настроек приложения", e);
        }
    }
}
