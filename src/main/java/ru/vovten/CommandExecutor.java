package ru.vovten;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.vovten.jms.DestinationType;
import ru.vovten.jms.JmsMessaging;
import ru.vovten.jms.message.JmsMessage;
import ru.vovten.jms.message.MessageType;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import java.io.IOException;

import static ru.vovten.jms.DestinationType.QUEUE;
import static ru.vovten.jms.DestinationType.TOPIC;
import static ru.vovten.jms.message.MessageType.OBJECT;
import static ru.vovten.jms.message.MessageType.TEXT;

/**
 * The executor of the commands from the console
 *
 * @author sbt-aleshkov-vyu
 * @created 22.08.2017.
 */
public class CommandExecutor {
    public static final String INFO_MSG = "To send a text message to the queue, run the command: 'java -jar JmsMessaging.jar -qst' \n" +
            "To send a object message to the queue, run the command: 'java -jar JmsMessaging.jar -qso' \n" +
            "To receive a message from the queue, run the command: 'java -jar JmsMessaging.jar -qr' \n" +
            "For a topic destination: 'java -jar JmsMessaging.jar -tst' for a text message or " +
            "'java -jar JmsMessaging.jar -tso' for object message and 'java -jar JmsMessaging.jar -tr' for receiving.";
    private static final Logger log = LoggerFactory.getLogger(CommandExecutor.class);

    /**
     * Executes the command from the console
     *
     * @param key key of command
     * @throws IOException
     * @throws JMSException
     */
    public static void execute(String key) throws IOException, JMSException {
        switch (key) {
            case "-qst":
                sendMessage(QUEUE, TEXT);
                break;

            case "-qso":
                sendMessage(QUEUE, OBJECT);
                break;

            case "-qr":
                receiveMessage(QUEUE);
                break;

            case "-tst":
                sendMessage(TOPIC, TEXT);
                break;

            case "-tso":
                sendMessage(TOPIC, OBJECT);
                break;

            case "-tr":
                receiveMessage(TOPIC);
                break;

            default:
                log.error("Unknown argument: " + key);
                log.info(INFO_MSG);
        }
    }

    private static void sendMessage(DestinationType destType, MessageType messageType) throws IOException {
        JmsMessaging messaging = JmsMessaging.create(destType);
        log.info(String.format("Connecting to a %s with the name %s", destType.description(), messaging.getDestinationName()));
        JmsMessage message = JmsMessage.createJmsMessage(messageType);
        log.info("Reading the message from message.xml: \n" + message.toString());
        messaging.send(message);
    }

    private static void receiveMessage(DestinationType destType) throws JMSException {
        JmsMessaging messaging = JmsMessaging.create(destType);
        log.info(String.format("Connecting to a %s with the name %s", destType.description(), messaging.getDestinationName()));
        log.info("Waiting for a message...");
        TextMessage message = (TextMessage) messaging.receive();
        log.info("The message has been received" + message.toString() + "\n\n The body of the message: \n" + message.getText());
    }
}
